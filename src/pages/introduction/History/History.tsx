import React from 'react';
import {
  Box,
  List,
  Grid,
  ListItem,
  ListItemText,
  Typography,
  ListSubheader,
  ListItemAvatar,
} from '@mui/material';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import Main from 'layouts/Main';
import OrgTree from 'react-org-tree';
import { Page } from 'common';
import navItems from 'layouts/navigation';

const navKey = 'introduction';
const itemId = 'History';
const contents = [
  {
    year: 2022,
    works: [
      { date: '2022.11.', do: '한국ESG운동본부 설립' },
      { date: '2022.12.', do: '사랑의 나눔(서경석 목사) MOU체결' },
    ],
  },
  {
    year: 2023,
    works: [
      { date: '2023.03.', do: '불용컴퓨터 등 기부 관련 협의(강남구청)' },
      { date: '2023.04.', do: '불용컴퓨터 등 기부물품 수거(서울관광재단)' },
      { date: '2023.04.', do: '불용컴퓨터 등 기부관련 협의(삼성전자)' },
    ],
  },
];

export const IntroductionHistoryPage = (): JSX.Element => {
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );

  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Box marginBottom={4} sx={{ pr: 2 }}>
              <Typography
                sx={{
                  textTransform: 'uppercase',
                  fontWeight: 'medium',
                }}
                gutterBottom
                color={'secondary'}
                align={'left'}
              >
                History
              </Typography>
              <Typography
                variant="h5"
                data-aos={'fade-up'}
                align={'left'}
                gutterBottom
                sx={{
                  fontWeight: 700,
                }}
              >
                국민과 함께한
                <br/>한국ESG운동본부의 발자취
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} md={6}>
            {contents.map((content,i) => {
              return (
                <List
                  sx={{
                    width: '100%',
                    maxWidth: 360,
                    bgcolor: 'background.paper',
                  }}
                  subheader={
                    <Typography
                      variant="h6"
                      color={'text.secondary'}
                      align={'left'}
                    >
                      {content.year}
                    </Typography>
                  }
                  data-aos="fade-up"
                  data-aos-delay={i * 100}
                  data-aos-offset={100}
                  data-aos-duration={600}
                >
                  {content.works.map((work) => {
                    return (
                      <ListItem alignItems="flex-start">
                        <ListItemAvatar>
                          <ArrowRightIcon />
                        </ListItemAvatar>
                        <ListItemText
                          primary={work.do}
                          secondary={
                            <React.Fragment>
                              <Typography
                                sx={{ display: 'inline' }}
                                component="span"
                                variant="body2"
                                color="text.primary"
                              >
                                {work.date}
                              </Typography>
                            </React.Fragment>
                          }
                        />
                      </ListItem>
                    );
                  })}
                </List>
              );
            })}
          </Grid>
        </Grid>
      </Page>
    </Main>
  );
};
