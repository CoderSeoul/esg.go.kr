import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import { useTheme } from '@mui/material/styles';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
const mock = [
  {
    title: '탄소 중립 실현을 향한 행보',
    subtitle:
      '한국ESG운동본부는 탄소 중립 실현을 목표로 삼고 있습니다. 우리는 지속 가능한 미래를 위해 탄소 배출을 최소화하고 재생 에너지와 같은 친환경적인 방법을 활용하여 사회적 가치를 창출할 것입니다.',
    icon: (
      <svg
        width={40}
        height={40}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z"
        />
      </svg>
    ),
  },
  {
    title: 'ESG 활동 지속적 확대',
    subtitle:
      '한국ESG운동본부는 ESG(환경, 사회, 지배구조)의 중요성을 인식하며 이를 기반으로 다양한 사업을 진행하고 있습니다. 앞으로도 ESG 활동을 지속적으로 확대하여 기업과 사회, 자연환경 모두에게 긍정적인 영향을 미칠 것입니다.',
    icon: (
      <svg
        width={40}
        height={40}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122"
        />
      </svg>
    ),
  },
  {
    title: '지속 가능한 경제성장을 위한 노력',
    subtitle:
      '한국ESG운동본부는 지속 가능한 경제성장을 위해 끊임없이 노력하고 있습니다. 우리는 기업이 사회적 가치를 창출하며 지속 가능한 경영을 실현할 수 있도록 지원하고, 이를 통해 국가 경쟁력을 강화하고 더 나은 미래를 만들어가고자 합니다.',
    icon: (
      <svg
        width={40}
        height={40}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
        />
      </svg>
    ),
  },
  {
    title: '환경 보호와 재생 에너지 발전을 위한 협력',
    subtitle:
      '한국ESG운동본부는 환경 보호와 재생 에너지 발전을 위해 다양한 협력을 추진하고 있습니다. 우리는 다양한 산업과 지역의 이해관계자들과 함께 지속 가능한 발전을 위한 프로젝트를 추진하고, 환경 보호와 재생 에너지 발전을 위한 인프라 구축에 노력할 것입니다.',
    icon: (
      <svg
        width={40}
        height={40}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"
        />
      </svg>
    ),
  },
];
const navKey = 'introduction';
const itemId = 'Mission';
export const IntroductionMissionPage = (): JSX.Element => {
  const theme = useTheme();
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );
  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box>
          <Box marginBottom={4}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
              align={'center'}
            >
              우리의 비전
            </Typography>
            <Typography
              variant="h4"
              align={'center'}
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              한국ESG운동본부, 지속 가능한 미래를 위한 동반성장과 혁신
            </Typography>
            <Typography
              variant="h6"
              align={'center'}
              color={'text.secondary'}
              data-aos={'fade-up'}
            >
              탄소 중립을 실현하며 환경과 경제의 가치를 통합하는 ESG,
              <br />
              함께 나아가는 삶의 질 향상을 위한 이념과 노력
            </Typography>
          </Box>
          <Grid container spacing={4}>
            {mock.map((item, i) => (
              <Grid key={i} item xs={12} md={6}>
                <ListItem
                  component="div"
                  disableGutters
                  data-aos={'fade-up'}
                  data-aos-delay={i * 100}
                  data-aos-offset={100}
                  data-aos-duration={600}
                  sx={{
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    padding: 0,
                  }}
                >
                  <Box
                    component={ListItemAvatar}
                    marginBottom={1}
                    minWidth={'auto !important'}
                  >
                    <Box color={theme.palette.primary.main}>{item.icon}</Box>
                  </Box>
                  <ListItemText
                    primary={item.title}
                    secondary={item.subtitle}
                    primaryTypographyProps={{
                      variant: 'h6',
                      gutterBottom: true,
                      align: 'left',
                    }}
                    secondaryTypographyProps={{ align: 'left' }}
                    sx={{
                      '& .MuiListItemText-primary': {
                        fontWeight: 700,
                      },
                      margin: 0,
                    }}
                  />
                </ListItem>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Page>
    </Main>
  );
};
