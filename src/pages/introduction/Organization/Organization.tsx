import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import Main from 'layouts/Main';
import OrgTree from 'react-org-tree';
import { Page } from 'common';
import navItems from 'layouts/navigation';

const navKey = 'introduction';
const itemId = 'Organization';
const orgData1 = {
  id: 0,
  label: '총회',
  children: [
    {
      id: 1,
      label: '이사회',
      children: [
        {
          id: 4,
          label: '사무국',
          children: [
            {
              id: 7,
              label: '기획본부',
            },
            {
              id: 5,
              label: '행정본부',
            },
            {
              id: 6,
              label: '대외협력본부',
            },
            {
              id: 6,
              label: '사업본부',
            },
          ],
        },
      ],
    },
  ],
};
const orgData2 = {
  id: 10,
  label: '감사',
  children: [
    {
      id: 11,
      label: '자문위원회',
      children: [
        {
          id: 12,
          label: '전문가그룹',
        },
      ],
    },
  ],
};
const horizontal = false;
const collapsable = false;
const expandAll = true;
export const IntroductionOrganizationPage = (): JSX.Element => {
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );

  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box>
          <Box marginBottom={4}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
            >
              Organization
            </Typography>
            <Typography
              variant="h5"
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              한국ESG운동본부의 
              <br/>구성과 조직 체계
            </Typography>
          </Box>
          <Box
            data-aos={'fade-up'}
            data-aos-delay={100}
            data-aos-offset={100}
            data-aos-duration={600}
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <OrgTree
              data={orgData1}
              horizontal={horizontal}
              collapsable={collapsable}
              expandAll={expandAll}
            />
            <OrgTree
              data={orgData2}
              horizontal={horizontal}
              collapsable={collapsable}
              expandAll={expandAll}
            />
          </Box>
        </Box>
      </Page>
    </Main>
  );
};
