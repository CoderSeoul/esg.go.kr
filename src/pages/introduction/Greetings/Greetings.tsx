import React from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import useMediaQuery from '@mui/material/useMediaQuery';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';

const navKey = 'introduction';
const itemId = 'Greetings';
export const IntroductionGreetingsPage = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );

  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box>
          <Grid container spacing={4} direction={isMd ? 'row' : 'column'}>
            <Grid
              item
              container
              alignItems={'center'}
              xs={12}
              md={8}
              data-aos={isMd ? 'fade-right' : 'fade-up'}
            >
              <Box>
                <Typography
                  sx={{
                    textTransform: 'uppercase',
                    fontWeight: 'medium',
                  }}
                  gutterBottom
                  color={'secondary'}
                >
                  인사말
                </Typography>
                <Typography
                  variant={'h4'}
                  gutterBottom
                  sx={{ fontWeight: 700 }}
                >
                  안녕하세요, <br />
                  <Typography
                    color={'primary'}
                    component={'span'}
                    variant={'inherit'}
                    sx={{
                      background: `linear-gradient(180deg, transparent 82%, ${alpha(
                        theme.palette.secondary.main,
                        0.3,
                      )} 0%)`,
                    }}
                  >
                    한국ESG운동본부
                  </Typography>
                  입니다.{' '}
                </Typography>
                <Typography
                  variant={'body1'}
                  component={'p'}
                  color={'text.secondary'}
                >
                  <br />
                  <br />
                  저희 단체는 탄소 중립실현과 기업의 지속 가능한 가치창출을 통해
                  국가 경쟁력을 강화하며, 환경친화적인 계몽 운동을 전개하여
                  인간의 삶의 질 향상에 기여하기위해 설립되었습니다.
                  <br />
                  <br />
                  ESG 시상 활동, 전문가 양성 및 교육사업, 인증 및 평가, 컨설팅
                  및 자문, Recycling 사업과 일자리 창출, 프로그램 운영, 탄소
                  배출권 거래 활동 등을 통해 이러한 목표를 달성하기 위해
                  노력하고 있습니다.
                  <br />
                  <br />
                  저희 단체는 현재 세대와 미래 세대가 더 나은 삶의 질을 누릴 수
                  있는 환경 조성에 최선을 다하고, 국제사회의 SDGs 추구에도
                  적극적으로 참여하여 더 나은 미래를 위한 노력을 지속적으로
                  진행할 것입니다.
                  <br />
                  <br />
                  감사합니다.
                </Typography>
              </Box>
            </Grid>
            <Grid
              item
              container
              justifyContent="center"
              alignItems="center"
              xs={12}
              md={4}
            >
              <Box maxWidth={490} width={1}>
                <Box
                  component={'img'}
                  src={'/images/main_image.png'}
                  width={1}
                  height={1}
                  sx={{
                    filter:
                      theme.palette.mode === 'dark'
                        ? 'brightness(0.8)'
                        : 'none',
                  }}
                />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Page>
    </Main>
  );
};
