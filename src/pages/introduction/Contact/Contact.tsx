import React from 'react';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import { useTheme } from '@mui/material/styles';
import { Page } from 'common';
import navItems from 'layouts/navigation';
import Container from 'components/Container';
import Main from 'layouts/Main';

const mock = [
  // {
  //   label: 'Phone',
  //   value: '+39 659-657-0133',
  //   icon: (
  //     <svg
  //       width={20}
  //       height={20}
  //       xmlns="http://www.w3.org/2000/svg"
  //       viewBox="0 0 20 20"
  //       fill="currentColor"
  //     >
  //       <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
  //     </svg>
  //   ),
  // },
  // {
  //   label: 'Email',
  //   value: 'hi@maccarianagency.com',
  //   icon: (
  //     <svg
  //       width={20}
  //       height={20}
  //       xmlns="http://www.w3.org/2000/svg"
  //       viewBox="0 0 20 20"
  //       fill="currentColor"
  //     >
  //       <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
  //       <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
  //     </svg>
  //   ),
  // },
  {
    label: 'Address',
    value: '서울시 영등포구 국회대로 70길 15-1, 극동VIP빌딩 1001호',
    icon: (
      <svg
        width={20}
        height={20}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path
          fillRule="evenodd"
          d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z"
          clipRule="evenodd"
        />
      </svg>
    ),
  },
];
const navKey = 'introduction';
const itemId = 'Contact';
export const IntroductionContactPage = (): JSX.Element => {
  const theme = useTheme();
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );
  const BottomSide = (): JSX.Element => {
    return (
      <Box
      >
        {mock.map((item, i) => (
          <Box
            key={i}
            component={ListItem}
            disableGutters
            width={'auto'}
            padding={0}
          >
            <Box
              component={ListItemAvatar}
              minWidth={'auto !important'}
              marginRight={2}
            >
              <Box
                component={Avatar}
                bgcolor={theme.palette.secondary.main}
                width={40}
                height={40}
              >
                {item.icon}
              </Box>
            </Box>
            <ListItemText primary={item.label} secondary={item.value} />
          </Box>
        ))}
      </Box>
    );
  };

  const TopSide = (): JSX.Element => {
    return (
      <iframe
        width="100%"
        height="100%"
        frameBorder="0"
        title="map"
        marginHeight={0}
        marginWidth={0}
        scrolling="no"
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3164.081392013523!2d126.9202807!3d37.52957860000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357c9f24e0304193%3A0x2cd29789cee306d7!2z7ISc7Jq47Yq567OE7IucIOyYgeuTse2PrOq1rCDqta3tmozrjIDroZw3MOq4uCAxNS0xIDEwMDHtmLg!5e0!3m2!1sko!2skr!4v1684023969870!5m2!1sko!2skr"
        style={{
          minHeight: 300,
          filter:
            theme.palette.mode === 'dark'
              ? 'grayscale(0.5) opacity(0.7)'
              : 'none',
        }}
      />
    );
  };

  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box
          sx={{
            width: 1,
            height: 1,
            overflow: 'hidden',
          }}
        >
          <Box marginBottom={4}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
            >
              CONTACT
            </Typography>
            <Typography
              variant="h5"
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              위치와 관련된 정보 및 안내
            </Typography>
          </Box>
          <Container paddingX={0} paddingY={0} maxWidth={{ sm: 1, md: 1236 }}>
            <TopSide />
            <BottomSide />
          </Container>
        </Box>
      </Page>
    </Main>
  );
};
