import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import useMediaQuery from '@mui/material/useMediaQuery';
import { alpha, useTheme } from '@mui/material/styles';
import { colors } from '@mui/material';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';


const mock = [
  {
    title: 'ESG 시상 활동',
    description:
      '한국ESG운동본부는 ESG 기반의 시상을 통해 우수 기업들의 ESG 실천을 인정하고 보상하는 활동을 수행합니다. 이를 통해 기업의 ESG 실천을 격려하고, 지속 가능한 경영의 중요성을 알리는 역할을 합니다.',
    illustration: '/images/certification.png',
    illustrationDark: '/images/certification.png',
    color: colors.blue[200],
  },
  {
    title: 'ESG 전문가 양성 및 교육사업',
    description: '다양한 사회 문제에 대한 대책을 마련합니다.',
    Content:
      '한국ESG운동본부는 ESG 전문가 양성과 교육사업을 통해 기업의 지속 가능한 경영 문화를 조성합니다. 전문가 양성과 교육을 통해 ESG에 대한 이해와 인식을 높이고, 지속 가능한 경영 문화를 확산하는데 기여합니다.',
    illustration: '/images/teacher.png',

    illustrationDark: '/images/teacher.png',

    color: colors.purple[200],
  },
  {
    title: 'ESG 인증, 평가 및 연구 사업',
    description:
      '한국ESG운동본부는 ESG 기준에 따른 평가와 연구를 통해 기업의 지속 가능한 경영을 실현합니다. ESG 인증, 평가 및 연구를 통해 기업의 ESG 실천을 검증하고, 지속 가능한 경영 실현을 위한 다양한 방안을 모색합니다.',
    illustration: '/images/research.png',
    illustrationDark: '/images/research.png',
    color: colors.indigo[200],
  },
  {
    title: 'Recycling 일자리 창출 사업',
    description:
      '한국ESG운동본부는 환경 보호와 일자리 창출을 동시에 실현하는 Recycling 사업을 수행합니다. 이를 통해 자원의 유용한 활용과 지속 가능한 경제 성장을 추구합니다.',
    illustration: '/images/recycle-symbol.png',
    illustrationDark: '/images/recycle-symbol.png',
    color: colors.green[200],
  },
];
const navKey = 'introduction';
const itemId = 'Business';
export const IntroductionBusinessPage = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );

  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box>
          <Box marginBottom={4}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
            >
              Bussiness
            </Typography>
            <Typography
              variant="h4"
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              우리의 사업 영역
            </Typography>
            <Typography
              variant="h6"
              color={'text.secondary'}
              data-aos={'fade-up'}
            >
              ESG 기반의 다양한 사업으로 새로운 미래를 열어가다
            </Typography>
          </Box>
          <Grid container spacing={isMd ? 8 : 4} paddingBottom={'80px'}>
            {mock.map((item, i) => (
              <Grid
                key={i}
                item
                xs={12}
                md={6}
                data-aos="fade-up"
                data-aos-delay={i * 100}
                data-aos-offset={100}
                data-aos-duration={600}
              >
                <Box
                  component={Card}
                  height={1}
                  bgcolor={alpha(item.color, 0.2)}
                  boxShadow={0}
                  sx={{
                    transform:
                      i % 2 === 1 ? { md: 'translateY(80px)' } : 'none',
                  }}
                >
                  <CardContent sx={{ padding: 4 }}>
                    <Box
                      display={'flex'}
                      justifyContent={'center'}
                      marginBottom={{ xs: 2, md: 4 }}
                    >
                      <Box
                        component={'img'}
                        loading="lazy"
                        src={
                          theme.palette.mode === 'light'
                            ? item.illustration
                            : item.illustrationDark
                        }
                        width={1}
                        sx={{maxWidth:200}}
                      />
                    </Box>
                    <Box>
                      <Typography
                        variant={'h6'}
                        gutterBottom
                        sx={{ fontWeight: 700 }}
                      >
                        {item.title}
                      </Typography>
                      <Typography color="text.secondary">
                        {item.description}
                      </Typography>
                      {/* <Button
                        size={'large'}
                        sx={{ marginTop: 2 }}
                        endIcon={
                          <Box
                            component={'svg'}
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            width={24}
                            height={24}
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M17 8l4 4m0 0l-4 4m4-4H3"
                            />
                          </Box>
                        }
                      >
                        좀 더 알아보기
                      </Button> */}
                    </Box>
                  </CardContent>
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Page>
    </Main>
  );
};
