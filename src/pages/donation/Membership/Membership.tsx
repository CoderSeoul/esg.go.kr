import React from 'react';
import Box from '@mui/material/Box';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';

import Container from 'components/Container';
import Description from './Description';
import Application from './Application';
const navKey = 'donation';
const itemId = 'Membership';
//https://github.com/dwyl/learn-to-send-email-via-google-script-html-no-server/blob/master/README.ko.md
export const DonationMembershipPage = (): JSX.Element => {
  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );
  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Container>
          <Description />
        </Container>
        <Box bgcolor={'alternate.main'}>
          <Container>
            <Application />
          </Container>
        </Box>
      </Page>
    </Main>
  );
};
