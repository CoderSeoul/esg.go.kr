import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import useMediaQuery from '@mui/material/useMediaQuery';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { handleFormSubmit } from 'common/sendMail';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
const validationSchema = yup.object({
  fullName: yup
    .string()
    .trim()
    .min(2, '유효한 이름을 입력해주세요')
    .max(50, '유효한 이름을 입력해주세요')
    .required('이름을 입력해주세요'),
  message: yup.string().trim().required('메시지를 입력해주세요'),
  email: yup
    .string()
    .trim()
    .email('유효한 이메일 주소를 입력해주세요')
    .required('이메일은 필수 항목입니다'),
});

const Application = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  const initialValues = {
    fullName: '',
    message: '',
    email: '',
    type: 'Membership',
  };
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  const handleSuccessClose = (
    event: React.SyntheticEvent | Event,
    reason?: string,
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setSuccessOpen(false);
  };

  const handleErrorClose = (
    event: React.SyntheticEvent | Event,
    reason?: string,
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setErrorOpen(false);
  };

  const onSubmit = (values) => {
    return values;
  };

  const formik = useFormik({
    initialValues,
    validationSchema: validationSchema,
    onSubmit,
  });
  const handleSubmitWithSnackbar = async (event) => {
    const form = event.target; // This is the form that was submitted
    const values = [...form.elements].reduce((obj, field) => {
      if (field.name) {
        obj[field.name] = field.value;
      }
      return obj;
    }, {});
    if (
      values.fullName !== initialValues.fullName &&
      values.email !== initialValues.email
    ) {
      await handleFormSubmit(event).then(() => {
        setSuccessOpen(true);
        formik.resetForm();
      });
    } else {
      setErrorOpen(true);
    }
  };
  useEffect(() => {
    const forms = document.querySelectorAll('form.gform');
    for (let i = 0; i < forms.length; i++) {
      forms[i].addEventListener('submit', handleSubmitWithSnackbar, false);
    }
  }, []);

  return (
    <Box maxWidth={800} margin={'0 auto'}>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={successOpen}
        autoHideDuration={6000}
        onClose={handleSuccessClose}
      >
        <Alert
          onClose={handleSuccessClose}
          severity="success"
          sx={{ width: '100%' }}
        >
          신청이 정상적으로 접수되었습니다. 연락처를 통해 결과를
          전달드리겠습니다.
        </Alert>
      </Snackbar>

      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={errorOpen}
        autoHideDuration={6000}
        onClose={handleErrorClose}
      >
        <Alert
          onClose={handleErrorClose}
          severity="error"
          sx={{ width: '100%' }}
        >
          필수 입력 요소가 누락되었습니다. 다시 확인해 주세요.
        </Alert>
      </Snackbar>
      <Box marginBottom={2}>
        <Typography
          variant={'h4'}
          sx={{ fontWeight: 700 }}
          gutterBottom
          align={'center'}
        >
          회원 가입 신청
        </Typography>
        <Typography color="text.secondary" align={'center'}>
          한국ESG운동본부의 멤버가 되어 ESG 실천에 참여해보세요. 아래 양식을
          작성하여 제출해주시면 멤버십 신청이 완료됩니다.
        </Typography>
      </Box>
      <Box
        component={'form'}
        className="gform"
        method="POST"
        target="iframe1"
        data-email="realhustlecoder@gmail.com"
        action="https://script.google.com/macros/s/AKfycbyoeWF9tC7pl1OfCigcwOEX9qb9rMADkcMf1PiOV5XUnQMhC0ycDhusj2HGVFPcVRXiHw/exec"
        sx={{
          '& .MuiOutlinedInput-root.MuiInputBase-multiline': {
            padding: 0,
          },
          '& .MuiOutlinedInput-input': {
            background: theme.palette.background.paper,
            padding: 2,
          },
        }}
      >
        <input type="hidden" name="type" value="Membership" />
        <Grid container spacing={isMd ? 4 : 2}>
          <Grid item xs={12} data-aos="fade-up">
            <Typography
              variant="subtitle1"
              color="text.primary"
              fontWeight={700}
              gutterBottom
            >
              이름
            </Typography>
            <TextField
              placeholder="이름을 기입해주세요."
              variant="outlined"
              size="medium"
              name="fullName"
              fullWidth
              type="text"
              value={formik.values.fullName}
              onChange={formik.handleChange}
              error={formik.touched.fullName && Boolean(formik.errors.fullName)}
              // @ts-ignore
              helperText={formik.touched.fullName && formik.errors.fullName}
            />
          </Grid>
          <Grid item xs={12} data-aos="fade-up">
            <Typography
              variant="subtitle1"
              color="text.primary"
              fontWeight={700}
              gutterBottom
            >
              이메일
            </Typography>
            <TextField
              placeholder="이메일 주소를 기입해주세요."
              variant="outlined"
              size="medium"
              name="email"
              fullWidth
              type="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              // @ts-ignore
              helperText={formik.touched.email && formik.errors.email}
            />
          </Grid>

          <Grid item xs={12} data-aos="fade-up">
            <Typography
              variant="subtitle1"
              color="text.primary"
              fontWeight={700}
              gutterBottom
            >
              기타 메시지
            </Typography>
            <TextField
              placeholder="기타 전달할 메시지를 기입해주세요."
              type="text"
              variant="outlined"
              name="message"
              fullWidth
              multiline
              rows={4}
              value={formik.values.message}
              onChange={formik.handleChange}
              error={formik.touched.message && Boolean(formik.errors.message)}
              // @ts-ignore
              helperText={formik.touched.message && formik.errors.message}
            />
          </Grid>
          <Grid item container justifyContent="center" xs={12}>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              size="large"
            >
              제출하기
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default Application;
