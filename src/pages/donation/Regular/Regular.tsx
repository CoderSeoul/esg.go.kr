import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import { alpha, useTheme } from '@mui/material/styles';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
import { handleFormSubmit } from 'common/sendMail';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const validationSchema = yup.object({
  fullName: yup
    .string()
    .trim()
    .min(2, '유효한 이름을 입력해주세요')
    .max(50, '유효한 이름을 입력해주세요')
    .required('이름을 입력해주세요'),
  email: yup
    .string()
    .trim()
    .email('유효한 이메일 주소를 입력해주세요')
    .required('이메일은 필수 항목입니다'),
  etc: yup.string().trim().max(500, '500자 이하로 입력해주세요'),
});

const navKey = 'donation';
const itemId = 'Regular';

export const DonationRegularPage = (): JSX.Element => {
  const theme = useTheme();

  const { title } = navItems[navKey].find((item) => item.id === itemId);
  const initialValues = {
    fullName: '',
    etc: '',
    email: '',
    type: 'Donation/Regular',
  };
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  const handleSuccessClose = (
    event: React.SyntheticEvent | Event,
    reason?: string,
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setSuccessOpen(false);
  };

  const handleErrorClose = (
    event: React.SyntheticEvent | Event,
    reason?: string,
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setErrorOpen(false);
  };

  const onSubmit = (values) => {
    return values;
  };

  const formik = useFormik({
    initialValues,
    validationSchema: validationSchema,
    onSubmit,
  });
  const handleSubmitWithSnackbar = async (event) => {
    const form = event.target; // This is the form that was submitted
    const values = [...form.elements].reduce((obj, field) => {
      if (field.name) {
        obj[field.name] = field.value;
      }
      return obj;
    }, {});
    if (
      values.fullName !== initialValues.fullName &&
      values.email !== initialValues.email
    ) {
      await handleFormSubmit(event).then(() => {
        setSuccessOpen(true);
        formik.resetForm();
      });
    } else {
      setErrorOpen(true);
    }
  };
  useEffect(() => {
    const forms = document.querySelectorAll('form.gform');
    for (let i = 0; i < forms.length; i++) {
      forms[i].addEventListener('submit', handleSubmitWithSnackbar, false);
    }
  }, []);

  return (
    <Main>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={successOpen}
        autoHideDuration={6000}
        onClose={handleSuccessClose}
      >
        <Alert
          onClose={handleSuccessClose}
          severity="success"
          sx={{ width: '100%' }}
        >
          신청이 정상적으로 접수되었습니다. 연락처를 통해 결과를
          전달드리겠습니다.
        </Alert>
      </Snackbar>

      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={errorOpen}
        autoHideDuration={6000}
        onClose={handleErrorClose}
      >
        <Alert
          onClose={handleErrorClose}
          severity="error"
          sx={{ width: '100%' }}
        >
          필수 입력 요소가 누락되었습니다. 다시 확인해 주세요.
        </Alert>
      </Snackbar>

      <Page navKey={navKey} title={title}>
        <Box>
          <Typography variant="h6" gutterBottom fontWeight={700}>
            <Typography
              color={'primary'}
              component={'span'}
              variant={'inherit'}
              sx={{
                background: `linear-gradient(180deg, transparent 82%, ${alpha(
                  theme.palette.secondary.main,
                  0.3,
                )} 0%)`,
              }}
            >
              정기후원
            </Typography>
            으로 지속 가능한 환경 운동에 함께하세요.
          </Typography>
          <Typography variant={'subtitle2'} color={'text.secondary'}>
            한국ESG운동본부는 여러분들의 소중한 후원을 통해 지속가능한 활동을
            이어나가고 있습니다.
            <br /> 정기후원을 통해 매월 일정한 금액을 후원해주시면,
            한국ESG운동본부가 추진하는 ESG 사업들의 성공적인 진행에 큰 도움을 줄
            수 있습니다.
            <br />
            <br />
            예금주 : 한국ESG운동본부
            <br />
            중소기업은행 : 448-053020-04-011
          </Typography>
          <Box paddingY={4}>
            <Divider />
          </Box>
          <Box
            component={'form'}
            className="gform"
            method="POST"
            target="iframe1"
            data-email="realhustlecoder@gmail.com"
            action="https://script.google.com/macros/s/AKfycbzx4LW1NoqPVLb4G1Y3jBvpuH1zqYdrrXt7Nc23MzpUAd5rdlJ2uWCNCNa6HLgIq2FKrQ/exec"
            sx={{
              '& .MuiOutlinedInput-root.MuiInputBase-multiline': {
                padding: 0,
              },
              '& .MuiOutlinedInput-input': {
                background: theme.palette.background.paper,
                padding: 2,
              },
            }}
          >
            <input type="hidden" name="type" value="Donation/Regular" />
            <Grid container spacing={4}>
              <Grid item xs={12} sm={6}>
                <Typography
                  variant={'subtitle2'}
                  sx={{ marginBottom: 2 }}
                  fontWeight={700}
                >
                  이름
                </Typography>
                <TextField
                  label="이름 *"
                  variant="outlined"
                  name={'fullName'}
                  fullWidth
                  value={formik.values.fullName}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.fullName && Boolean(formik.errors.fullName)
                  }
                  // @ts-ignore
                  helperText={formik.touched.fullName && formik.errors.fullName}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography
                  variant={'subtitle2'}
                  sx={{ marginBottom: 2 }}
                  fontWeight={700}
                >
                  이메일
                </Typography>
                <TextField
                  label="이메일 *"
                  variant="outlined"
                  name="email"
                  fullWidth
                  type="email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  // @ts-ignore
                  helperText={formik.touched.email && formik.errors.email}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography
                  variant={'subtitle2'}
                  sx={{ marginBottom: 2 }}
                  fontWeight={700}
                >
                  기타 정보
                </Typography>
                <TextField
                  label="기타 정보"
                  variant="outlined"
                  name={'etc'}
                  multiline
                  rows={5}
                  fullWidth
                  value={formik.values.etc}
                  onChange={formik.handleChange}
                  error={formik.touched.etc && Boolean(formik.errors.etc)}
                  // @ts-ignore
                  helperText={formik.touched.etc && formik.errors.etc}
                />
              </Grid>
              <Grid item xs={12}>
                <Divider />
              </Grid>

              <Grid item container xs={12}>
                <Box
                  display="flex"
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ xs: 'stretched', sm: 'center' }}
                  justifyContent={'space-between'}
                  width={1}
                  margin={'0 auto'}
                >
                  <Box/>
                  <Button size={'large'} variant={'contained'} type={'submit'}>
                    제출하기
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Page>
    </Main>
  );
};
