import React, { useMemo, useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Chip from '@mui/material/Chip';
import Pagination from '@mui/material/Pagination';
import CardContent from '@mui/material/CardContent';
import navItems from 'layouts/navigation';
import Grid from '@mui/material/Grid';
import Main from 'layouts/Main';
import { alpha, useTheme } from '@mui/material/styles';
import { Page } from 'common';
import { content } from './content';

const navKey = 'activity';
const itemId = 'Schedule';
const pageSize = 4;

export const ActivitySchedulePage = (): JSX.Element => {
  const theme = useTheme();
  const [currentPage, setCurrentPage] = useState(1);

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * pageSize;
    const lastPageIndex = firstPageIndex + pageSize;
    return content.slice(firstPageIndex, lastPageIndex);
  }, [currentPage]);

  const { title } = navItems[navKey].find((item) => item.id === itemId);
  if (content.length > 0) {
    return (
      <Main>
        <Page navKey={navKey} title={title}>
          <Box>
            <Grid container spacing={4}>
              {currentTableData.map((item, i) => (
                <Grid item xs={12} md={6} key={i}>
                  <Box
                    component={'a'}
                    href={''}
                    display={'block'}
                    width={1}
                    height={1}
                    sx={{
                      textDecoration: 'none',
                      transition: 'all .2s ease-in-out',
                      '&:hover': {
                        transform: `translateY(-${theme.spacing(1 / 2)})`,
                      },
                    }}
                  >
                    <Box component={Card} width={1} height={1}>
                      <CardMedia
                        image={item.image}
                        title={item.title}
                        sx={{
                          height: { xs: 300, md: 360 },
                          position: 'relative',
                          filter:
                            theme.palette.mode === 'dark'
                              ? 'brightness(0.7)'
                              : 'none',
                        }}
                      />
                      <CardContent
                        sx={{
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Box
                          display={'flex'}
                          justifyContent={'center'}
                          flexWrap={'wrap'}
                        >
                          {item.tags.map((item) => (
                            <Chip
                              key={item}
                              label={item}
                              size={'small'}
                              sx={{ marginBottom: 1, marginRight: 1 }}
                            />
                          ))}
                        </Box>
                        <Typography
                          variant={'h6'}
                          fontWeight={700}
                          align={'center'}
                          sx={{ textTransform: 'uppercase' }}
                        >
                          {item.title}
                        </Typography>
                        <Box marginY={1}>
                          <Typography
                            variant={'caption'}
                            align={'center'}
                            color={'text.secondary'}
                            component={'i'}
                          >
                            {item.author.name} - {item.date}
                          </Typography>
                        </Box>
                        <Typography color="text.secondary" align={'center'}>
                          {item.description}
                        </Typography>
                      </CardContent>
                    </Box>
                  </Box>
                </Grid>
              ))}
            </Grid>
            <Box
              display={'flex'}
              justifyContent={'center'}
              alignItems={{ xs: 'center' }}
              marginBottom={4}
              marginTop={4}
            >
              <Box>
                <Pagination
                  page={currentPage}
                  count={content.length / pageSize}
                  onChange={(event, page) => {
                    setCurrentPage(page);
                  }}
                />
              </Box>
            </Box>
          </Box>
        </Page>
      </Main>
    );
  } else {
    return (
      <Main>
        <Page navKey={navKey} title={title}>
          <Box>
            <img
              src={'/images/no_result.gif'}
              loading="lazy"
              style={{ width: '100%' }}
              alt={'no_result'}
            />
          </Box>
        </Page>
      </Main>
    );
  }
};
