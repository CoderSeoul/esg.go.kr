import React, { useEffect, useMemo, useState } from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
import useMediaQuery from '@mui/material/useMediaQuery';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import Pagination from '@mui/material/Pagination';
import { content } from './content';
import Papa from 'papaparse';
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.background.level2,
    color: theme.palette.common.black,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const navKey = 'notice';
const itemId = 'Inform';
const pageSize = 4;

export const NoticeInformPage = (): JSX.Element => {
  const languageList = ['아이디', '설명'];
  const navigate = useNavigate();
  const theme = useTheme();
  const [currentPage, setCurrentPage] = useState(1);

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * pageSize;
    const lastPageIndex = firstPageIndex + pageSize;
    return content.slice(firstPageIndex, lastPageIndex);
  }, [currentPage]);

  const { title } = navItems[navKey].find((item) => item.id === itemId);
  const move = ({ id }) => {
    // 두번재 인자의 state 속성에 원하는 파라미터를 넣어준다. (id, job을 넣어봤다)
    navigate(`/${navKey}/${itemId}/${id}`, {
      state: {
        id: 1,
      },
    });
  };

  // useEffect(() => {
  //   Papa.parse(
  //     'https://docs.google.com/spreadsheets/d/e/2PACX-1vQoAGR8GD0DwSjeYwXdbaNOf-2-SlvgZnhHo3-5a8vb3LDWfKDQIXT6GXCZCGZEiQKnitN02KJR2zxa/pub?gid=0&single=true&output=csv',
  //     {
  //       download: true,
  //       header: true,
  //       complete: (results) => {
  //         console.log(results.data);
  //       },
  //     },
  //   );
  // }, []);
  if (content.length > 0) {
    return (
      <Main>
        <Page navKey={navKey} title={title}>
          <TableContainer component={Paper} elevation={0}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">제목</StyledTableCell>
                  <StyledTableCell align="center" width={100}>
                    작성자
                  </StyledTableCell>
                  <StyledTableCell align="center" width={100}>
                    작성일
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {currentTableData.map((row, index) => (
                  <StyledTableRow
                    key={row.title}
                    onClick={() => move({ id: index })}
                  >
                    <StyledTableCell align="center">
                      {row.title}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {row.author}
                    </StyledTableCell>
                    <StyledTableCell align="center">{row.date}</StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
            <Box
              display={'flex'}
              justifyContent={'center'}
              alignItems={{ xs: 'center' }}
              marginBottom={4}
              marginTop={4}
            >
              <Box>
                <Pagination
                  page={currentPage}
                  count={content.length / pageSize}
                  onChange={(event, page) => {
                    setCurrentPage(page);
                  }}
                />
              </Box>
            </Box>
          </TableContainer>
        </Page>
      </Main>
    );
  } else {
    return (
      <Main>
        <Page navKey={navKey} title={title}>
          <Box>
            <img
              src={'/images/no_result.gif'}
              loading="lazy"
              style={{ width: '100%' }}
              alt={'no_result'}
            />
          </Box>
        </Page>
      </Main>
    );
  }
};
