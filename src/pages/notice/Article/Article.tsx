import React, { useMemo, useState } from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
import { useNavigate } from 'react-router-dom';

const navKey = 'notice';
const itemId = 'Article';

export const NoticeArticlePage = (): JSX.Element => {
  const navigate = useNavigate();
  const theme = useTheme();
  const [currentPage, setCurrentPage] = useState(1);

  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );

  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box/>
      </Page>
    </Main>
  );
};
