import React from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
import useMediaQuery from '@mui/material/useMediaQuery';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';

const navKey = 'business';
const itemId = 'Consulting';
export const BusinessConsultingPage = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  const photos = [
    {
      src: 'https://images.unsplash.com/photo-1540575467063-178a50c2df87?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      source:
        'https://images.unsplash.com/photo-1540575467063-178a50c2df87?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      rows: 1,
      cols: 2,
    },
    {
      src: 'https://images.unsplash.com/photo-1553877522-43269d4ea984?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      source:
        'https://images.unsplash.com/photo-1553877522-43269d4ea984?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      rows: 1,
      cols: 1,
    },
    {
      src: 'https://images.unsplash.com/photo-1573166364266-356ef04ae798?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80',
      source:
        'https://images.unsplash.com/photo-1573166364266-356ef04ae798?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80',
      rows: 1,
      cols: 1,
    },
    {
      src: 'https://images.unsplash.com/photo-1532102235608-dc8fc689c9ab?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      source:
        'https://images.unsplash.com/photo-1532102235608-dc8fc689c9ab?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      rows: 1,
      cols: 2,
    },
  ];

  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );
  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box
          sx={{
            position: 'relative',
            '&::after': {
              position: 'absolute',
              content: '""',
              width: '30%',
              zIndex: 1,
              top: 0,
              right: 0,
              height: '100%',
              backgroundSize: '18px 18px',
              backgroundImage: `radial-gradient(${alpha(
                theme.palette.primary.dark,
                0.4,
              )} 20%, transparent 20%)`,
              opacity: 0.2,
            },
          }}
        >
          <Box position="relative" zIndex={2}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
            >
              Consulting
            </Typography>
            <Typography
              variant="h4"
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              ESG 경영 전략 수립을 위한
              <br />
              전문 컨설팅 서비스
            </Typography>
            <Typography
              variant="body1"
              color={'text.secondary'}
              data-aos={'fade-up'}
            >
              한국ESG운동본부는 ESG 적극 실천을 위해 컨설팅과 자문 서비스를
              제공합니다.
              <br />
              <br />
              우리의 전문가들은 탄소중립, 지속가능 경영 전략 등 다양한 ESG 관련
              이슈에 대해 교육과 상담을 통해 기업체와 공공기관이 더 나은 세상을
              만들 수 있도록 돕고 있습니다.
              <br />
              <br />
              또한, 한국ESG운동본부는 불용물품 수거 운동을 전개하고 있으며, 이를
              통해 지구환경을 위해 노력하고 있습니다. 한국ESG운동본부는 또한
              SDGs 실천 가이드를 제공하며, 다양한 ESG 활동에 대한 정보를
              제공하는 홈페이지, 네이버 카페, 블로그 등을 운영하고 있습니다.{' '}
              <br />
              <br />
              함께 ESG 적극 실천을 통해 아름다운 지구를 지키는 운동에
              참여해보세요.
            </Typography>
          </Box>
        </Box>
        <Box>
          <ImageList
            variant="quilted"
            cols={3}
            rowHeight={isMd ? 300 : 200}
            gap={isMd ? 16 : 4}
            sx={{overflow:'hidden'}}

          >
            {photos.map((item, i) => (
              <ImageListItem
                key={i}
                cols={item.cols}
                rows={item.rows}
                data-aos="fade-up"
                data-aos-delay={i * 100}
                data-aos-offset={100}
                data-aos-duration={600}
              >
                <img
                  height={'100%'}
                  width={'100%'}
                  src={item.src}
                  alt="..."
                  loading="lazy"
                  style={{
                    objectFit: 'cover',
                    filter:
                      theme.palette.mode === 'dark'
                        ? 'brightness(0.7)'
                        : 'none',
                    cursor: 'poiner',
                    borderRadius: 8,
                  }}
                />
              </ImageListItem>
            ))}
          </ImageList>
        </Box>
      </Page>
    </Main>
  );
};
