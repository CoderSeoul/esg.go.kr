import React from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
import useMediaQuery from '@mui/material/useMediaQuery';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';

const navKey = 'business';
const itemId = 'Certification';
export const BusinessCertificationPage = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  const photos = [
    {
      src: 'https://img.hankyung.com/photo/202112/01.28406222.1.jpg',
      source: 'https://img.hankyung.com/photo/202112/01.28406222.1.jpg',
      rows: 1,
      cols: 2,
    },
    {
      src: 'https://images.pexels.com/photos/142497/pexels-photo-142497.jpeg?cs=srgb&dl=pexels-mali-maeder-142497.jpg&fm=jpg',
      source:
        'https://images.pexels.com/photos/142497/pexels-photo-142497.jpeg?cs=srgb&dl=pexels-mali-maeder-142497.jpg&fm=jpg',
      rows: 1,
      cols: 1,
    },
    {
      src: 'https://images.unsplash.com/photo-1589330694653-ded6df03f754?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1816&q=80',
      source:
        'https://images.unsplash.com/photo-1589330694653-ded6df03f754?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1816&q=80',
      rows: 1,
      cols: 1,
    },
    {
      src: 'https://assets.maccarianagency.com/backgrounds/img21.jpg',
      source: 'https://assets.maccarianagency.com/backgrounds/img21.jpg',
      rows: 1,
      cols: 2,
    },
  ];

  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );
  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box
          sx={{
            position: 'relative',
            '&::after': {
              position: 'absolute',
              content: '""',
              width: '30%',
              zIndex: 1,
              top: 0,
              right: 0,
              height: '100%',
              backgroundSize: '18px 18px',
              backgroundImage: `radial-gradient(${alpha(
                theme.palette.primary.dark,
                0.4,
              )} 20%, transparent 20%)`,
              opacity: 0.2,
            },
          }}
        >
          <Box position="relative" zIndex={2}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
            >
              Certification
            </Typography>
            <Typography
              variant="h4"
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              ESG 연구 및 자격증 취득을 통한
              <br />
              환경 개선과 사회적 가치 창출
            </Typography>
            <Typography
              variant="body1"
              color={'text.secondary'}
              data-aos={'fade-up'}
            >
              ESG 분야에서는 다양한 자격증 취득이 가능합니다. 이러한 자격증은
              기업이 ESG 분야에서 지속 가능한 경영을 실천하기 위한 노력을
              인정받을 수 있는 수단이 됩니다.
              <br />
              <br />
              ESG 분야에서의 연구와 분석, 그리고 자격증 취득을 통해 기업의 경영
              혁신을 이루고, 환경 개선과 사회적 가치 창출에 기여할 수 있다는
              것을 알고 있습니다. 그래서 우리는 ESG 분야의 전문가들과 함께 ESG
              연구와 자격증 취득 프로그램을 개발하고 있습니다.
              <br />
              <br />
              우리의 ESG 연구 프로그램은 ESG 관련 분석 기법과 평가 방법론, ESG
              리서치 등을 다루며, 기업의 ESG 성과를 분석하고 개선하는 방법을
              제공합니다. 또한, 우리는 ESG 관련 자격증 취득 프로그램을 제공하고
              있으며, 이는 기업의 ESG 분야에서의 인증을 받을 수 있는 수단입니다.
              <br />
              <br />
              우리의 ESG 연구 및 자격증 취득 프로그램은 ESG 분야의 전문가들이
              개발하고 운영합니다. 또한, 우리의 프로그램은 국내외에서 인정받는
              ESG 관련 자격증 취득을 지원합니다. 우리와 함께 ESG 분야에서 경영
              혁신을 이루고, 환경 개선과 사회적 가치 창출에 기여해보세요.
            </Typography>
          </Box>
        </Box>
        <Box>
          <ImageList
            variant="quilted"
            cols={3}
            rowHeight={isMd ? 300 : 200}
            gap={isMd ? 16 : 4}
            sx={{overflow:'hidden'}}
          >
            {photos.map((item, i) => (
              <ImageListItem
                key={i}
                cols={item.cols}
                rows={item.rows}
                data-aos="fade-up"
                data-aos-delay={i * 100}
                data-aos-offset={100}
                data-aos-duration={600}
              >
                <img
                  height={'100%'}
                  width={'100%'}
                  src={item.src}
                  alt="..."
                  loading="lazy"
                  style={{
                    objectFit: 'cover',
                    filter:
                      theme.palette.mode === 'dark'
                        ? 'brightness(0.7)'
                        : 'none',
                    cursor: 'poiner',
                    borderRadius: 8,
                  }}
                />
              </ImageListItem>
            ))}
          </ImageList>
        </Box>
      </Page>
    </Main>
  );
};
