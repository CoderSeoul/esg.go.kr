import React from 'react';
import { alpha, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Page } from 'common';
import Main from 'layouts/Main';
import navItems from 'layouts/navigation';
import useMediaQuery from '@mui/material/useMediaQuery';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';

const navKey = 'business';
const itemId = 'Education';
export const BusinessEducationPage = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  const photos = [
    {
      src: 'https://images.unsplash.com/photo-1526253038957-bce54e05968e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      source:
        'https://images.unsplash.com/photo-1526253038957-bce54e05968e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
      rows: 1,
      cols: 2,
    },
    {
      src: 'https://images.pexels.com/photos/142497/pexels-photo-142497.jpeg?cs=srgb&dl=pexels-mali-maeder-142497.jpg&fm=jpg',
      source:
        'https://images.pexels.com/photos/142497/pexels-photo-142497.jpeg?cs=srgb&dl=pexels-mali-maeder-142497.jpg&fm=jpg',
      rows: 1,
      cols: 1,
    },
    {
      src: 'https://images.unsplash.com/photo-1456735190827-d1262f71b8a3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2048&q=80',
      source:
        'https://images.unsplash.com/photo-1456735190827-d1262f71b8a3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2048&q=80',
      rows: 1,
      cols: 1,
    },
    {
      src: 'https://images.unsplash.com/photo-1541829070764-84a7d30dd3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80',
      source:
        'https://images.unsplash.com/photo-1541829070764-84a7d30dd3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80',
      rows: 1,
      cols: 2,
    },
  ];

  const { title } = navItems[navKey].find(
    (item) => item.id === itemId,
  );
  return (
    <Main>
      <Page navKey={navKey} title={title}>
        <Box
          sx={{
            position: 'relative',
            '&::after': {
              position: 'absolute',
              content: '""',
              width: '30%',
              zIndex: 1,
              top: 0,
              right: 0,
              height: '100%',
              backgroundSize: '18px 18px',
              backgroundImage: `radial-gradient(${alpha(
                theme.palette.primary.dark,
                0.4,
              )} 20%, transparent 20%)`,
              opacity: 0.2,
            },
          }}
        >
          <Box position="relative" zIndex={2}>
            <Typography
              sx={{
                textTransform: 'uppercase',
                fontWeight: 'medium',
              }}
              gutterBottom
              color={'secondary'}
            >
              Education
            </Typography>
            <Typography
              variant="h4"
              data-aos={'fade-up'}
              gutterBottom
              sx={{
                fontWeight: 700,
              }}
            >
              지속 가능한 경영을 실천하고, <br />
              ESG에 대한 관심을 높이는 교육
            </Typography>
            <Typography
              variant="body1"
              color={'text.secondary'}
              data-aos={'fade-up'}
            >
              ESG 환경 개선을 위한 교육 프로그램"은 지속 가능한 경영을 위해 ESG
              요소를 적극적으로 고려하는 기업들이 늘어나면서, ESG 교육 수요도
              증가하고 있습니다. 우리 교육 프로그램은 이에 대한 답으로
              시작되었습니다.
              <br />
              <br />
              교육 프로그램에서는 환경, 사회, 지배구조 등 ESG에 대한 개념과
              중요성을 이해할 수 있으며, 각 분야별 최신 동향 및 법률 등에 대한
              정보를 제공합니다. 또한, 다양한 케이스 스터디를 통해 ESG 경영의
              핵심 요소와 기업의 비즈니스와의 관련성을 배울 수 있습니다.
              <br />
              <br />
              우리의 교육 프로그램은 단순히 이론적인 부분뿐만 아니라, 실제로
              ESG를 실천할 수 있는 방법을 제시하고, 참여자들과 함께 구체적인
              계획을 세워나갑니다. 함께 배우고, 함께 실천하는 ESG 환경 개선의
              가치를 실감할 수 있습니다.
              <br />
              <br />
              ESG는 기업 뿐만 아니라 모든 개인과 조직에게 중요한 가치입니다.
              우리 교육 프로그램을 통해 ESG 개념을 이해하고, 현실적인 방안을
              제시받아 함께 환경을 개선하는 데 기여하시길 바랍니다.
            </Typography>
          </Box>
        </Box>
        <Box>
          <ImageList
            variant="quilted"
            cols={3}
            rowHeight={isMd ? 300 : 200}
            gap={isMd ? 16 : 4}
            sx={{overflow:'hidden'}}

          >
            {photos.map((item, i) => (
              <ImageListItem
                key={i}
                cols={item.cols}
                rows={item.rows}
                data-aos="fade-up"
                data-aos-delay={i * 100}
                data-aos-offset={100}
                data-aos-duration={600}
              >
                <img
                  height={'100%'}
                  width={'100%'}
                  src={item.src}
                  alt="..."
                  loading="lazy"
                  style={{
                    objectFit: 'cover',
                    filter:
                      theme.palette.mode === 'dark'
                        ? 'brightness(0.7)'
                        : 'none',
                    cursor: 'poiner',
                    borderRadius: 8,
                  }}
                />
              </ImageListItem>
            ))}
          </ImageList>
        </Box>
      </Page>
    </Main>
  );
};
