export { default as Hero } from './Hero';
export { default as Services } from './Services';
export { default as GetStarted } from './GetStarted';
export { default as PopularNews } from './PopularNews';
export { default as Categories } from './Categories';
export { default as Articles } from './News';
