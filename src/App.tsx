import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from './Routes';
import Page from './components/Page';
import { Helmet } from 'react-helmet-async';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'aos/dist/aos.css';

const App = (): JSX.Element => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <link rel="shortcut icon" href="images/LOGOV2.png" />
        <meta
          name="robots"
          content="index,follow,max-snippet:-1, max-image-preview:large, max-video-preview:-1"
        />
        <meta
          name="naver-site-verification"
          content="c451bc17aa06d5ad935123ea58e29d22ad8cee85"
        />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta name="theme-color" content="#ffffff" />
        <meta
          name="description"
          content="한국ESG운동본부: 탄소 배출권 거래와 리사이클 사업으로 지속 가능한 사회 창출에 기여"
        />
        <meta
          name="keywords"
          content="ESG, 리사이클, 탄소 배출권 거래, 지속 가능한 사회, 일자리 창출"
        />
        <meta http-equiv="Content-Language" content="ko" />
        <meta property="og:locale" content="ko_KR" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="images/LOGOV2.png" />
        <meta property="og:image:width" content="1200" />{' '}
        {/* Replace with actual width */}
        <meta property="og:image:height" content="630" />{' '}
        {/* Replace with actual height */}
        <meta property="og:site_name" content="한국ESG운동본부" />
        <meta
          property="og:title"
          content="한국ESG운동본부: 지속 가능한 사회를 위한 우리의 노력"
        />
        <meta
          property="og:description"
          content="한국ESG운동본부는 탄소 배출권 거래, 리사이클 사업, 프로그램 운영을 통해 지속 가능한 사회를 만들어가는 데 기여하고 있습니다."
        />
        <meta property="og:url" content="https://go-esg.org/" />
        <base href="/" />
        <link
          rel="stylesheet"
          as="style"
          href="https://cdn.jsdelivr.net/gh/orioncactus/pretendard@v1.3.6/dist/web/variable/pretendardvariable-dynamic-subset.css"
          media="all"
        />
        <title>한국ESG운동본부: 지속 가능한 사회를 위한 우리의 노력</title>
      </Helmet>
      <Page>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </Page>
    </>
  );
};

export default App;
