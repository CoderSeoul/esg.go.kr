import { PaletteMode } from '@mui/material';

export const light = {
  alternate: {
    main: '#FAFAFB',
    dark: '#C8E5D9',
  },
  cardShadow: 'rgba(21, 161, 133, .11)',
  mode: 'light' as PaletteMode,
  primary: {
    main: '#28B463',
    light: '#52BE80',
    dark: '#1D8348',
    contrastText: '#fff',
  },
  secondary: {
    light: '#F8C471',
    main: '#F5B041',
    dark: '#D35400',
    contrastText: 'rgba(0, 0, 0, 0.87)',
  },
  text: {
    primary: '#333333',
    secondary: '#677788',
    dark:'#000'
  },
  divider: 'rgba(0, 0, 0, 0.12)',
  background: {
    paper: '#ffffff',
    default: '#ffffff',
    level2: '#f5f5f5',
    level1: '#ffffff',
    mainImage:'url(\'/images/bg_human.png\')'
  },
};

export const dark = {
  alternate: {
    main: '#253238',
    dark: '#1B2529',
  },
  cardShadow: 'rgba(0, 0, 0, .11)',
  common: {
    black: '#333333',
    white: '#fff',
  },
  mode: 'dark' as PaletteMode,
  primary: {
    main: '#28B463',
    light: '#52BE80',
    dark: '#1D8348',
    contrastText: '#fff',
  },
  secondary: {
    light: '#F8C471',
    main: '#F5B041',
    dark: '#D35400',
    contrastText: 'rgba(0, 0, 0, 0.87)',
  },
  text: {
    primary: '#EEEEEF',
    secondary: '#AEB0B4',
    dark:'#fff'
  },
  divider: 'rgba(255, 255, 255, 0.12)',
  background: {
    paper: '#293742',
    default: '#17202B',
    level2: '#333',
    level1: '#2D3748',
    mainImage:'url(\'/images/bg_human_b.png\')'
  },
};
