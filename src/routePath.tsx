import React from 'react';
import MainPage from 'pages/main';
import {
  IntroductionMissionPage,
  IntroductionBusinessPage,
  IntroductionContactPage,
  IntroductionGreetingsPage,
  IntroductionHistoryPage,
  IntroductionOrganizationPage,
} from 'pages/introduction';
import {
  BusinessCertificationPage,
  BusinessConsultingPage,
  BusinessEducationPage,
} from 'pages/business';
import {
  DonationRegularPage,
  DonationDonatePage,
  DonationMembershipPage,
} from 'pages/donation';
import { ActivityNewsPage, ActivitySchedulePage } from 'pages/activity';
import {
  NoticeInformPage,
  NoticeArticlePage,
} from 'pages/notice';

const routes = [
  {
    path: '/',
    renderer: (params = {}): JSX.Element => <MainPage {...params} />,
  },
  {
    path: '/introduction/greetings',
    renderer: (params = {}): JSX.Element => (
      <IntroductionGreetingsPage {...params} />
    ),
  },
  {
    path: '/introduction/mission',
    renderer: (params = {}): JSX.Element => (
      <IntroductionMissionPage {...params} />
    ),
  },
  {
    path: '/introduction/business',
    renderer: (params = {}): JSX.Element => (
      <IntroductionBusinessPage {...params} />
    ),
  },
  {
    path: '/introduction/history',
    renderer: (params = {}): JSX.Element => (
      <IntroductionHistoryPage {...params} />
    ),
  },
  {
    path: '/introduction/organization',
    renderer: (params = {}): JSX.Element => (
      <IntroductionOrganizationPage {...params} />
    ),
  },
  {
    path: '/introduction/contact',
    renderer: (params = {}): JSX.Element => (
      <IntroductionContactPage {...params} />
    ),
  },
  {
    path: '/business/certification',
    renderer: (params = {}): JSX.Element => (
      <BusinessCertificationPage {...params} />
    ),
  },
  {
    path: '/business/consulting',
    renderer: (params = {}): JSX.Element => (
      <BusinessConsultingPage {...params} />
    ),
  },
  {
    path: '/business/education',
    renderer: (params = {}): JSX.Element => (
      <BusinessEducationPage {...params} />
    ),
  },
  {
    path: '/activity/news',
    renderer: (params = {}): JSX.Element => <ActivityNewsPage {...params} />,
  },
  {
    path: '/activity/schedule',
    renderer: (params = {}): JSX.Element => (
      <ActivitySchedulePage {...params} />
    ),
  },
  {
    path: '/donation/membership',
    renderer: (params = {}): JSX.Element => (
      <DonationMembershipPage {...params} />
    ),
  },
  {
    path: '/donation/donate',
    renderer: (params = {}): JSX.Element => <DonationDonatePage {...params} />,
  },
  {
    path: '/donation/regular',
    renderer: (params = {}): JSX.Element => <DonationRegularPage {...params} />,
  },
  {
    path: '/notice/inform',
    renderer: (params = {}): JSX.Element => (
      <NoticeInformPage {...params} />
    ),
  },
  {
    path: '/notice/esg',
    renderer: (params = {}): JSX.Element => (
      <NoticeInformPage {...params} />
    ),
  },
  {
    path: '/notice/:article_id',
    renderer: (params): JSX.Element => (
      <NoticeArticlePage {...params} />
    ),
  },
];

export default routes;
