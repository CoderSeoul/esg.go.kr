import React, { useState, useEffect } from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Container from 'components/Container';
import navItems from 'layouts/navigation';
import { alpha } from '@mui/material/styles';

interface Props {
  navKey:string;
  title: string;
  children: React.ReactNode;
}

export const Page = ({ navKey,title, children }: Props): JSX.Element => {
  const [activeLink, setActiveLink] = useState('');
  useEffect(() => {
    setActiveLink(window && window.location ? window.location.pathname : '');
  }, []);

  const theme = useTheme();
  const page = navItems[navKey];

  return (
    <>
      <Box
        position={'relative'}
        sx={{
          backgroundColor: theme.palette.background.default,
          background:
            'url(https://static.vecteezy.com/system/resources/previews/008/711/757/non_2x/abstract-gradient-wave-curve-layers-on-light-blue-hologram-color-background-wavy-stripes-pattern-design-modern-and-minimal-concept-can-use-for-template-brochure-poster-banner-web-print-vector.jpg) no-repeat center',
          backgroundSize: 'cover',
        }}
        paddingY={4}
      >
        <Container sx={{ position: 'relative', zIndex: 2 }}>
          <Typography
            variant="h4"
            fontWeight={700}
            gutterBottom
            sx={{ color: 'text.dark' }}
          >
            {title}
          </Typography>
        </Container>
        <Box
          sx={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: 1,
            height: 1,
            background: `linear-gradient(to bottom, ${alpha(
              theme.palette.background.default,
              0.5,
            )}, ${alpha(theme.palette.background.default, 1)} 120%)`,
            zIndex: 1,
          }}
        />
      </Box>
      <Container paddingTop={'0 !important'} marginTop={4}>
        <Grid container spacing={4}>
          <Grid item xs={0} md={3}>
            <Card sx={{ boxShadow: 3 }}>
              <List
                disablePadding
                sx={{
                  display: { xs: 'none', md: 'flex' },
                  flexDirection: { md: 'column' },
                  overflow: 'auto',
                  flexWrap: 'nowrap',
                  width: '100%',
                  paddingY: { xs: 3, md: 4 },
                  paddingX: { xs: 4, md: 0 },
                }}
              >
                {page.map((item) => (
                  <ListItem
                    key={item.href}
                    component={'a'}
                    href={item.href}
                    disableGutters
                    sx={{
                      marginRight: { xs: 2, md: 0 },
                      flex: 0,
                      paddingX: { xs: 0, md: 3 },
                      borderLeft: {
                        xs: 'none',
                        md: '2px solid transparent',
                      },
                      borderLeftColor: {
                        md:
                          activeLink === item.href
                            ? theme.palette.primary.main
                            : 'transparent',
                      },
                    }}
                  >
                    <Typography
                      variant="subtitle1"
                      noWrap
                      color={
                        activeLink === item.href
                          ? 'text.primary'
                          : 'text.secondary'
                      }
                    >
                      {item.title}
                    </Typography>
                  </ListItem>
                ))}
              </List>
            </Card>
          </Grid>
          <Grid item xs={12} md={9}>
            <Card sx={{ boxShadow: 3, padding: 4 }}>{children}</Card>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};
