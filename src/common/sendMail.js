function disableAllButtons(form) {
  const buttons = form.querySelectorAll('button');
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].disabled = true;
  }
}
function getFormData(form) {
  const elements = form.elements;
  let honeypot;

  const fields = Object.keys(elements)
    .filter(function (k) {
      if (elements[k].name === 'honeypot') {
        honeypot = elements[k].value;
        return false;
      }
      return true;
    })
    .map(function (k) {
      if (elements[k].name !== undefined) {
        return elements[k].name;
        // special case for Edge's html collection
      } else if (elements[k].length > 0) {
        return elements[k].item(0).name;
      }
    })
    .filter(function (item, pos, self) {
      return self.indexOf(item) == pos && item;
    });

  const formData = {};
  fields.forEach(function (name) {
    const element = elements[name];

    // singular form elements just have one value
    formData[name] = element.value;

    // when our element has multiple items, get their values
    if (element.length) {
      const data = [];
      for (let i = 0; i < element.length; i++) {
        const item = element.item(i);
        if (item.checked || item.selected) {
          data.push(item.value);
        }
      }
      formData[name] = data.join(', ');
    }
  });

  return { data: formData, honeypot: honeypot };
}
export function handleFormSubmit(event) {
  return new Promise((resolve, reject) => {
    event.preventDefault();
    const form = event.target;
    const formData = getFormData(form);
    const data = formData.data;

    if (formData.honeypot) {
      return resolve(false);
    }

    disableAllButtons(form);
    const url = form.action;
    const xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        form.reset();
        const formElements = form.querySelector('.form-elements');
        if (formElements) {
          formElements.style.display = 'none';
        }
        const thankYouMessage = form.querySelector('#thankyou_message');
        if (thankYouMessage) {
          thankYouMessage.style.display = 'block';
        }
        resolve(true);
      } else {
        resolve(false);
      }
    };
    const encoded = Object.keys(data)
      .map(function (k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]);
      })
      .join('&');
    xhr.send(encoded);
  });
}