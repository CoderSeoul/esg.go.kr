import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';

import NavItem from './components/NavItem';

interface Props {
  pages: {
    introduction: Array<PageItem>;
    business: Array<PageItem>;
    activity: Array<PageItem>;
    donation: Array<PageItem>;
    // awards: Array<PageItem>;
    notice: Array<PageItem>;
  };
}

const SidebarNav = ({ pages }: Props): JSX.Element => {
  const theme = useTheme();
  const { mode } = theme.palette;

  const {
    introduction: introductionPages,
    business: businessPages,
    activity: activityPages,
    donation: donationPages,
    // awards: awardsPages,
    notice: noticePages,
  } = pages;

  return (
    <Box >
      <Box width={1} paddingX={2} paddingY={1}>
        <Box
          display={'flex'}
          component="a"
          href="/"
          title="theFront"
          width={{ xs: 220, md: 270 }}
        >
          <Box
            component={'img'}
            src={
              mode === 'light' ? '/images/LOGO2_H.png' : '/images/LOGO2_W_H.png'
            }
            height={1}
            width={1}
          />
        </Box>
      </Box>
      <Box paddingX={2} paddingY={2}>
        <Box>
          <NavItem title={'소개'} items={introductionPages} />
        </Box>
        <Box>
          <NavItem title={'사업안내'} items={businessPages} />
        </Box>
        <Box>
          <NavItem title={'활동상황'} items={activityPages} />
        </Box>
        <Box>
          <NavItem title={'후원안내'} items={donationPages} />
        </Box>
        {/* <Box>
          <NavItem title={'시상제도'} items={awardsPages} />
        </Box> */}
        <Box>
          <NavItem title={'참여/알림'} items={noticePages} />
        </Box>
      </Box>
    </Box>
  );
};

export default SidebarNav;
