const navItems = {
  introduction: [
    {
      id: 'Greetings',
      title: '인사말',
      href: '/introduction/greetings',
    },
    {
      id: 'Mission',
      title: '설립목적',
      href: '/introduction/mission',
    },
    {
      id: 'Business',
      title: '주요사업',
      href: '/introduction/business',
    },
    {
      id: 'History',
      title: '연혁',
      href: '/introduction/history',
    },
    {
      id: 'Organization',
      title: '조직도',
      href: '/introduction/organization',
    },
    {
      id: 'Contact',
      title: '오시는 길',
      href: '/introduction/contact',
    },
  ],
  business: [
    {
      id: 'Consulting',
      title: '컨설팅/자문',
      href: '/business/consulting',
    },
    {
      id: 'Education',
      title: '교육',
      href: '/business/education',
    },
    {
      id: 'Certification',
      title: '연구/자격증',
      href: '/business/certification',
    },
  ],
  activity: [
    {
      id: 'News',
      title: '활동 소식',
      href: '/activity/news',
    },
    {
      id: 'Schedule',
      title: '행사일정',
      href: '/activity/schedule',
    },
  ],
  donation: [
    {
      id: 'Membership',
      title: '회원 가입안내',
      href: '/donation/membership',
    },
    {
      id: 'Donate',
      title: '후원하기',
      href: '/donation/donate',
    },
    {
      id: 'Regular',
      title: '정기후원',
      href: '/donation/regular',
    },
  ],
  // awards: [
  //   {
  //     id: 'Winners',
  //     title: '수상작',
  //     subtitle: 'ESG 기업 평가 대상 수상작 소개',
  //     href: '/awards/winners',
  //   },
  //   {
  //     id: 'Evaluation-criteria',
  //     title: '평가항목',
  //     subtitle: 'ESG 기업 평가의 주요 항목 소개',
  //     href: '/awards/evaluation-criteria',
  //   },
  //   {
  //     id: 'Evaluation-method',
  //     title: '평가 방법',
  //     subtitle: 'ESG 기업 평가의 평가 방법 소개',
  //     href: '/awards/evaluation-method',
  //   },
  //   {
  //     id: 'Awards-photos',
  //     title: '시상 사진',
  //     subtitle: 'ESG 기업 평가 시상식의 사진 소개',
  //     href: '/awards/photos',
  //   },
  // ],
  notice: [
    {
      id: 'Inform',
      title: '공지사항',
      href: '/notice/inform',
    },
    {
      id: 'ESG',
      title: 'ESG 뉴스',
      href: '/notice/esg',
    },
  ],
};

export default navItems;
